package com.maurilio.cfs.game;

import static org.lwjgl.glfw.GLFW.*;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.GL;

import static org.lwjgl.opengl.GL46.*;

import org.joml.Vector3d;

import com.maurilio.cfs.engine.core.Camera;
import com.maurilio.cfs.engine.core.Input;
import com.maurilio.cfs.engine.core.Loader;
import com.maurilio.cfs.engine.core.Model;
import com.maurilio.cfs.engine.core.WindowManager;
import com.maurilio.cfs.engine.core.shaders.StaticShader;

public class Game {

    public static void main(String[] args) {

        Camera camera = new Camera(new Vector3d(0, 0, -5), new Vector3d(), new Vector3d(1, 1, 1), 90.0, 0.1, 2000.0);
        
        WindowManager gameWM = new WindowManager(1024, 768, "CFS ENGINE", true) {

            private Model model;
            //private StaticShader shader;
            
            @Override
            public void onStart() {
                try {
                    model = new Model(GL_STATIC_DRAW);
                    model.setTestModel();
                    Loader.loadModel(model);
                    //model.loadFromOjb("C:\\Users\\mauri\\Downloads\\cube.obj");
                    //model.load();
                    //shader = new StaticShader();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            
            @Override
            public void onUpdate(double delta) {

                if (Input.isKeyPressed(GLFW.GLFW_KEY_ESCAPE)) {
                    glfwSetWindowShouldClose(this.getWindow(), true);
                }

                double RY = (Input.isKeyPressed(GLFW.GLFW_KEY_Q) ? 2 : 0) - (Input.isKeyPressed(GLFW.GLFW_KEY_E) ? 2 : 0);
                double MZ = (Input.isKeyPressed(GLFW.GLFW_KEY_W) ? 5 : 0) - (Input.isKeyPressed(GLFW.GLFW_KEY_S) ? 5 : 0);
                double MX = (Input.isKeyPressed(GLFW.GLFW_KEY_A) ? 5 : 0) - (Input.isKeyPressed(GLFW.GLFW_KEY_D) ? 5 : 0);

                if (RY != 0) {
                    camera.getMatrix().rotateLocalY(-RY * delta);
                }
                if (MZ != 0 || MX != 0) {
                    camera.getMatrix().translateLocal(new Vector3d(MX * delta, 0, MZ * delta));
                }
                
                //camera.applyPerspectiveMatrix((double) getWindowHeight() / getWindowWidth(), shader.getUniformLocation("modelViewMatrix"));
                
                //shader.useThisProgram();
                
                if (model.isLoaded()) {
                    model.drawModel();
                }
                
                //shader.useNoProgram();

            }
            
            @Override
            public void beforeTerminate() {
                Loader.cleanAll();
                //shader.cleanThisProgram();
            }

        };
        gameWM.init();
    }
}
