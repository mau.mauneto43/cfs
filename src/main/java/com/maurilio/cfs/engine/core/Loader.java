package com.maurilio.cfs.engine.core;

import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;
import org.lwjgl.BufferUtils;
import static org.lwjgl.opengl.GL46.*;


public class Loader {
    
    private static List<Integer> vaoList = new ArrayList<Integer>();
    private static List<Integer> vboList = new ArrayList<Integer>();
    
    public static void loadModel(Model model) {
        int vaoId = glGenVertexArrays();
        glBindVertexArray(vaoId);
        int verticesVBO = createVBO(0, 3, model.getVertexArray());
        int indexVBO = createIndexVBO(model.getIndexArray());
        vaoList.add(vaoId);
        vboList.add(verticesVBO);
        vboList.add(indexVBO);
        model.setVaoId(vaoId);
        model.setLoaded(true);
        glBindVertexArray(0);
    }
    
    public static int createVBO(int attr, int size, float[] data) {
        int vboId = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, vboId);
        FloatBuffer floatBuffer = BufferUtils.createFloatBuffer(data.length);
        floatBuffer.put(data);
        floatBuffer.flip();
        glBufferData(GL_ARRAY_BUFFER, floatBuffer, GL_STATIC_DRAW);
        glVertexAttribPointer(attr, size, GL_FLOAT, false, 0,0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        return vboId;
    }
    
    public static int createIndexVBO(int[] data) {
        int vboId = glGenBuffers();
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboId);
        IntBuffer intBuffer = BufferUtils.createIntBuffer(data.length);
        intBuffer.put(data);
        intBuffer.flip();
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, intBuffer, GL_STATIC_DRAW);
        return vboId;
    }
    
    public static void cleanAll() {
        glDeleteVertexArrays(toIntArray(vaoList));
        glDeleteBuffers(toIntArray(vboList));
    }
    
    public static float[] toFloatArray(List<Float> list) {
        float[] ret = new float[list.size()];
        for (int i=0; i<list.size(); i++) {
            ret[i] = list.get(i).floatValue();
        }
        return ret;
    }
    
    public static int[] toIntArray(List<Integer> list) {
        int[] ret = new int[list.size()];
        for (int i=0; i<list.size(); i++) {
            ret[i] = list.get(i).intValue();
        }
        return ret;
    }

}