package com.maurilio.cfs.engine.core;

import static org.lwjgl.glfw.GLFW.*;

public class Input {
    
    private static WindowManager wm;
    
    public static boolean isKeyPressed(int keycode) {
    	return glfwGetKey(wm.getWindow(), keycode) == GLFW_PRESS;
    }
    
    public static void updateWindowManager(WindowManager wm) {
    	Input.wm = wm;
    };
}