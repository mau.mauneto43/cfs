package com.maurilio.cfs.engine.core;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL46.*;

public class Model {
    
    private String name;
    private float[] vertexArray;
    private float[] vertexColorArray;
    private float[] normalArray;
    private float[] uvArray;
    private int[] indexArray;
    private Boolean smooth;
    private Boolean loaded;
    private Integer useMode;
    
    private Integer vaoId;
    
    /***
     * 
     * @param mode should be one of [GL_STREAM_DRAW, GL_STATIC_DRAW, GL_DYNAMIC_DRAW]
     */
    public Model(int mode) {
        this.useMode = mode;
    }
    
    public void setTestModel() {
        vertexArray = new float[] {
            -0.5f, 0.5f, 0f,//v0
            -0.5f, -0.5f, 0f,//v1
            0.5f, -0.5f, 0f,//v2
            0.5f, 0.5f, 0f,//v3
        };
        
        indexArray = new int[] {
            0,1,3,//top left triangle (v0, v1, v3)
            3,1,2//bottom right triangle (v3, v1, v2)
        };
    }
    
    public void loadFromOjb(String filePath) throws IOException {
        List<Float> vertexList = new ArrayList<Float>();
        List<Float> vertexColorList = new ArrayList<Float>();
        List<Float> normalList = new ArrayList<Float>();
        List<Float> uvList = new ArrayList<Float>();
        List<Integer> indexList = new ArrayList<Integer>();
        BufferedReader br = new BufferedReader(new FileReader(filePath));
        
        String line = br.readLine();
        while (line != null) {
            // # COMMENT
            if (line.startsWith("# ")) {
                line = br.readLine();
                continue;
            }
            
            // MATERIAL
            if (line.startsWith("mtllib ")) {
                line = br.readLine();
                continue;
            }
            
            // OBJECT NAME
            if (line.startsWith("o ")) {
                this.name = line.replace("o ", "");
                line = br.readLine();
                continue;
            }
            
            // VEXTEX
            if (line.startsWith("v ")) {
                String temp[] = line.replace("v ", "").split(" ");
                for (int i=0; i<temp.length; i++) {
                    if (i < 3) {
                        vertexList.add(Float.parseFloat(temp[i]));
                    } else {
                        vertexColorList.add(Float.parseFloat(temp[i]));
                    }
                }
                line = br.readLine();
                continue;
            }
            
            // VEXTEX NORMAL
            if (line.startsWith("vn ")) {
                String temp[] = line.replace("vn ", "").split(" ");
                for (String t : temp) {
                    normalList.add(Float.parseFloat(t));
                }
                line = br.readLine();
                continue;
            }
            
            // VEXTEX UV
            if (line.startsWith("vt ")) {
                String temp[] = line.replace("vt ", "").split(" ");
                for (String t : temp) {
                    uvList.add(Float.parseFloat(t));
                }
                line = br.readLine();
                continue;
            }
            
            // SMOOTHING
            if (line.startsWith("s ")) {
                smooth = line.equals("s 1");
                line = br.readLine();
                continue;
            }
            
            // FACES
            if (line.startsWith("f ")) {
                String temp[] = line.replace("f ", "").split(" ");
                for (int i=0; i<temp.length; i++) {
                    String[] trio = temp[i].split("/");
                    indexList.add(Integer.parseInt(trio[0]));
                }
                line = br.readLine();
                continue;
            }
        }
        
        // LIST TO ARRAY
        vertexArray = Loader.toFloatArray(vertexList);
        vertexColorArray = Loader.toFloatArray(vertexColorList);
        normalArray = Loader.toFloatArray(normalList);
        uvArray = Loader.toFloatArray(uvList);
        indexArray = Loader.toIntArray(indexList);
    }
    
    public void drawModel() throws RuntimeException {
        if (!isLoaded()) {
            throw new RuntimeException("Cannot draw unloaded Model!");
        }

        glBindVertexArray(vaoId);
        glEnableVertexAttribArray(0);
        glDrawElements(GL_TRIANGLES, indexArray.length, GL_UNSIGNED_INT, 0);
        glDisableVertexAttribArray(0);
        glBindVertexArray(0);
    }
    
    public boolean isLoaded() {
        return loaded;
    }

    public float[] getVertexArray() {
        return vertexArray;
    }

    public void setVertexArray(float[] vertexArray) {
        this.vertexArray = vertexArray;
    }

    public int[] getIndexArray() {
        return indexArray;
    }

    public void setIndexArray(int[] indexArray) {
        this.indexArray = indexArray;
    }

    public Boolean getLoaded() {
        return loaded;
    }

    public void setLoaded(Boolean loaded) {
        this.loaded = loaded;
    }

    public Integer getVaoId() {
        return vaoId;
    }

    public void setVaoId(Integer vaoId) {
        this.vaoId = vaoId;
    }
    
}