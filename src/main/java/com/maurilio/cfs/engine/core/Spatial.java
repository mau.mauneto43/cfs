package com.maurilio.cfs.engine.core;

import org.joml.Matrix4d;
import org.joml.Vector3d;

public class Spatial {
	
    private Matrix4d matrix;
    private double[] matrixArray;

    public Spatial() {
        this.matrix = new Matrix4d();
        this.matrixArray = new double[16];
    }

    public Spatial(Vector3d pos, Vector3d rot, Vector3d scl) {
        this.matrix = new Matrix4d(
                scl.x,	0, 	0,	pos.x,
                0,	scl.y, 	0,	pos.y,
                0,	0,  scl.z,	pos.z,
                0,	0,	0,	1
        );
        this.matrix.rotateXYZ(new Vector3d(Math.toRadians(rot.x),Math.toRadians(rot.y),Math.toRadians(rot.z)));
        this.matrixArray = new double[16];
    }

    public Vector3d getTranslation() {
        Vector3d vector = null;
        return matrix.getTranslation(vector);
    }

    public Vector3d getScale() {
        Vector3d vector = null;
        return matrix.getScale(vector);
    }

    public Vector3d getRotation() {
        Vector3d vector = null;
        return matrix.getEulerAnglesXYZ(vector);
    }

    public Matrix4d getMatrix() {
        return this.matrix;
    }

    public double[] getMatrixArray() {
        return matrix.get(matrixArray);
    }

}
