package com.maurilio.cfs.engine.core;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import static org.lwjgl.opengl.GL46.*;

public abstract class ShaderProgram {
    
    private int programId;
    private int vertexShaderId;
    private int fragmentShaderId;
    
    public ShaderProgram(String vertexFile, String fragmentFile) {
        vertexShaderId = loadShader(vertexFile, GL_VERTEX_SHADER);
        fragmentShaderId = loadShader(fragmentFile, GL_FRAGMENT_SHADER);
        programId = glCreateProgram();
        glAttachShader(programId, vertexShaderId);
        glAttachShader(programId, fragmentShaderId);
        glLinkProgram(programId);
        glValidateProgram(programId);
        bindAttributes();
    }
    
    public final int getUniformLocation(String uniformName) {
        return glGetUniformLocation(programId, uniformName);
    }
    
    protected abstract void bindAttributes();
    
    protected void bindAttribute(int attrId, String variableName) {
        glBindAttribLocation(programId, attrId, variableName);
    }
    
    public void useThisProgram() {
        glUseProgram(programId);
    }

    public void useNoProgram() {
        glUseProgram(0);
    }
    
    public void cleanThisProgram() {
        useNoProgram();
        glDetachShader(programId, vertexShaderId);
        glDetachShader(programId, fragmentShaderId);
        glDeleteShader(vertexShaderId);
        glDeleteShader(fragmentShaderId);
        glDeleteProgram(programId);
    }
    
    private static int loadShader(String file, int type) {
        StringBuilder shaderSource = new StringBuilder();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line;
            while ((line = reader.readLine()) != null) {
                shaderSource.append(line).append("//\n");
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        int shaderID = glCreateShader(type);
        glShaderSource(shaderID, shaderSource);
        glCompileShader(shaderID);
        if (glGetShaderi(shaderID, GL_COMPILE_STATUS) == GL_FALSE) {
            System.out.println(glGetShaderInfoLog(shaderID, 500));
            System.err.println("Could not compile shader!");
            System.exit(-1);
        }
        return shaderID;
    }
    
}