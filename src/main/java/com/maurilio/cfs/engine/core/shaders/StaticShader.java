package com.maurilio.cfs.engine.core.shaders;

import com.maurilio.cfs.engine.core.ShaderProgram;

public class StaticShader extends ShaderProgram {

    private static final String VERTEX_SHADER_PATH = "src/main/java/com/maurilio/cfs/engine/core/shaders/vertexShader.glsl";
    private static final String FRAGMENT_SHADER_PATH = "src/main/java/com/maurilio/cfs/engine/core/shaders/fragmentShader.glsl";
    
    private int modelViewMatrixUniformLocation;
    
    public StaticShader() {
        super(VERTEX_SHADER_PATH, FRAGMENT_SHADER_PATH);
    }
    
    @Override
    protected void bindAttributes() {
        super.bindAttribute(0, "position");
    }

}