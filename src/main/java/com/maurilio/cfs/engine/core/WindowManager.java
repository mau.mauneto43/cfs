package com.maurilio.cfs.engine.core;

import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;
import org.lwjgl.system.*;

import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL46.*;

public abstract class WindowManager {
    
    private static final long NANOSECOND = 1000000000L;

    private static int fps;
	
    private int windowHeight;
    private int windowWidth;
    private String title;
    private boolean vSync;

    private long window;

    public WindowManager(int width, int height, String title, boolean vSync) {
        this.windowWidth = width > 100 ? width: 100;
        this.windowHeight = height > 100 ? height : 100;
        this.title = title;
        this.vSync = vSync;
    }

    public void init() {
        GLFWErrorCallback.createPrint(System.err).set();
        
        if ( !glfwInit() )
            throw new IllegalStateException("Unable to initialize GLFW");

        glfwDefaultWindowHints();
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
        glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);
        
        this.window = glfwCreateWindow(windowWidth, windowHeight, title, MemoryUtil.NULL, MemoryUtil.NULL);
        if ( window == MemoryUtil.NULL )
            throw new RuntimeException("Failed to create the GLFW window");

        Input.updateWindowManager(this);
        
        glfwSetFramebufferSizeCallback(window, (window, width, height) -> {
            this.windowWidth = width;
            this.windowHeight = height;
        });
        
        glfwSetKeyCallback(window, (window, key, scancode, action, mods) -> {
            //Input.addInput(new InputEvent(window, key, scancode, action, mods));
        });

        GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        glfwSetWindowPos(
            window,
            (vidmode.width() - getWindowWidth()) / 2,
            (vidmode.height() - getWindowHeight()) / 2
        );
        
        glfwMakeContextCurrent(window);
        GL.createCapabilities();
        
        Callback debugProcCallback = GLUtil.setupDebugMessageCallback();
        
        if (isvSync())
            glfwSwapInterval(1);
        
        glfwShowWindow(window);

        glClearColor(0.5f, 0.7f, 1.0f, 1.0f);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_STENCIL_TEST);
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
        
        onStart();
        
        loop();
        terminate();
    }

    public void loop() {

    	int frameCount = 0;
    	long lastTime = System.nanoTime();
    	double cummulativeTime = 0;
    	
        while (!glfwWindowShouldClose(window)) {
            
            glfwSwapBuffers(window);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glfwPollEvents();

            long currentTime = System.nanoTime();
            long passedTime = currentTime - lastTime;
            lastTime = currentTime;
            cummulativeTime += passedTime / (double) NANOSECOND;

            if (cummulativeTime > 1.0) {
                fps = frameCount;
                frameCount = 0;
                cummulativeTime -= 1.0;
                glfwSetWindowTitle(getWindow(), title + " FPS:" + fps);
            }


            this.onUpdate(passedTime / (double) NANOSECOND);

            frameCount++;
        }
    }

    public void terminate() {
        beforeTerminate();
        
        glfwFreeCallbacks(this.window);
        glfwDestroyWindow(this.window);

        glfwTerminate();
        glfwSetErrorCallback(null).free();
    }

    public abstract void onUpdate(double delta);

    public abstract void onStart();
    
    public abstract void beforeTerminate();
    
    public int getWindowHeight() {
        return this.windowHeight;
    }
    
    public void setWindowHeight(int windowHeight) {
        this.windowHeight = windowHeight;
    }
    
    public int getWindowWidth() {
        return this.windowWidth;
    }
    
    public void setWindowWidth(int windowWidth) {
        this.windowWidth = windowWidth;
    }
    
    public long getWindow() {
        return this.window;
    }

    public String getTitle() {
            return title;
    }

    public void setTitle(String title) {
            this.title = title;
    }

    public boolean isvSync() {
            return vSync;
    }

    public static int getFps() {
            return fps;
    }

}

