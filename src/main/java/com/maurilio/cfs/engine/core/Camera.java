package com.maurilio.cfs.engine.core;

import java.util.Objects;

import org.joml.Matrix4d;
import org.joml.Vector3d;

import static org.lwjgl.opengl.GL46.*;

public class Camera extends Spatial {

    private static Camera current;
    private double fov;
    private double zNear;
    private double zFar;
    private double[] perspectMatrixArray;

    public Camera(Vector3d translation, Vector3d rotation, Vector3d scale, double fov, double zNear, double zFar) {
        super(translation, rotation, scale);
        this.fov = Math.toRadians(fov);
        this.perspectMatrixArray = new double[16];
        this.zNear = zNear;
        this.zFar = zFar;
        if (Objects.isNull(current)) {
                setCurrent(this);
        }
    }

    public static Camera getCurrent() {
        return current;
    }

    public static void setCurrent(Camera current) {
        Camera.current = current;
    }

    public double getFov() {
        return Math.toDegrees(fov);
    }

    public void setFov(double fov) {
        this.fov = Math.toRadians(fov);
    }

    public double getzNear() {
        return zNear;
    }

    public void setzNear(double zNear) {
        this.zNear = zNear;
    }

    public double getzFar() {
        return zFar;
    }

    public void setzFar(double zFar) {
        this.zFar = zFar;
    }

    public void applyPerspectiveMatrix(double aspectRatio, int uniformLocation) {
        Matrix4d perspectMatrix = new Matrix4d();
        perspectMatrix.setPerspective(fov, aspectRatio, getzNear(), getzFar());
        perspectMatrix.mul(getMatrix());
        perspectMatrix.get(perspectMatrixArray);
        glUniformMatrix4dv(uniformLocation, false, perspectMatrixArray);
    }
	
}
